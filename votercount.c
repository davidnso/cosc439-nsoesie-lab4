/******************************************************************************

                            Online C Compiler.
                Code, Compile, Run and Debug C program online.
Write your code in this editor and press "Run" button to compile and execute it.

*******************************************************************************/
#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>

pthread_mutex_t mutex1 = PTHREAD_MUTEX_INITIALIZER;
int votes = 0;

void *countvotes(void *param) {
  int i;

  for (i = 0; i < 1000000; i++) {
    pthread_mutex_lock( &mutex1 );
    votes += 1;
    pthread_mutex_unlock( &mutex1 );

  }
  return NULL;
}

int main() {
  pthread_t tid1, tid2, tid3, tid4, tid5;
  pthread_create(&tid1, NULL, countvotes, NULL);
  pthread_create(&tid2, NULL, countvotes, NULL);
  pthread_create(&tid3, NULL, countvotes, NULL);
  pthread_create(&tid4, NULL, countvotes, NULL);
  pthread_create(&tid5, NULL, countvotes, NULL);

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);
  pthread_join(tid3, NULL);
  pthread_join(tid4, NULL);
  pthread_join(tid5, NULL);

  printf("Vote total is %d\n", votes);
  return 0;
}

