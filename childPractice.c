#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void childProcess(); 
void ParentProcess();

int main(){
	pid_t pid;
	pid = fork();

	if(pid==0){
	childProcess();	
	}else{
	ParentProcess();	
	}
	return 0;
}

void childProcess(){
printf("hello there child");}

void ParentProcess(){
printf("hello there parent");}
