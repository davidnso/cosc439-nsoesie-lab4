#include <stdio.h>
#include <pthread.h>
#include <stdlib.h>
#include <semaphore.h>

sem_t mutex1;
int count = 1;
int votes = 0;

void *countvotes(void *param) {
  int i;
  for (i = 0; i < 1000000; i++) {
    sem_wait( &mutex1 );
    votes += 1;
    sem_post( &mutex1 );

  }

  return NULL;
}

int main() {
  sem_init(&mutex1, 0, count);
  pthread_t tid1, tid2, tid3, tid4, tid5;
  pthread_create(&tid1, NULL, countvotes, NULL);
  pthread_create(&tid2, NULL, countvotes, NULL);
  pthread_create(&tid3, NULL, countvotes, NULL);
  pthread_create(&tid4, NULL, countvotes, NULL);
  pthread_create(&tid5, NULL, countvotes, NULL);

  pthread_join(tid1, NULL);
  pthread_join(tid2, NULL);
  pthread_join(tid3, NULL);
  pthread_join(tid4, NULL);
  pthread_join(tid5, NULL);
sem_destroy(&mutex1);
  printf("Vote total is %d\n", votes);
  return 0;
}